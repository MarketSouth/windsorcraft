<?php
/**
 * Windsor Products plugin for Craft CMS 3.x
 *
 * Fetch products from backend
 *
 * @link      https://www.hotzone.in
 * @copyright Copyright (c) 2020 Priti Rathod
 */

namespace windsor\windsorproducts\controllers;

use windsor\windsorproducts\WindsorProducts;
use craft\base\Element;
use Craft;
use craft\web\Controller;

/**
 * Product Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Priti Rathod
 * @package   WindsorProducts
 * @since     1.0.0
 */
class ProductController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'get-ajax-category', 'test-scripts', 'fetch-products', 'fetch-categories', 'delete-products', 'delete-categories', 'aus-fetch-products', 'aus-fetch-categories', 'aus-delete-products', 'aus-delete-categories'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/windsor-products/product
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the ProductController actionIndex() method';

        return $result;
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/windsor-products/product/fetch-products
     *
     * @return mixed
     */
    public function actionFetchProducts()
    {

      ini_set('max_execution_time', 0);
      //set_time_limit(0);
      try {
        $username = 'gss';
        $password = '1912FrogMutiny';

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://exo.api.myob.com/stocksearchtemplate/25",
          CURLOPT_USERPWD => $username . ":" . $password,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "x-myobapi-exotoken:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
            "x-myobapi-key: 5de3bswg3ej8xxkcf5dynh39"
          ),
        ));

        $response = curl_exec($curl);
        //echo curl_errno($curl);
        // echo curl_error($curl);
        curl_close($curl);

        $products = json_decode($response);
        //echo "<pre>"; print_r($products); exit;
        $productStructure = array();
        //check product variants exist or not if not then add/or Update
        $productVariants = [];
        $productVariantsNew = [];

        if($products->data) {
          foreach ($products->data as $key => $product) {

              //Checking for category exist or we have to create one.
            // if ($key > 10) {
              // continue;
            //  }


        //Prepare data here only and create in next loop.
              $productCategory = '';
              $categories =  \craft\elements\Category::find()
              ->group('productCategories')
			  ->siteId(1)
              ->where(['content.field_categoryGroupNo' => trim($product[0])])->one();

              if($categories && $categories->id){

				$productCategory = $categories->id;
			  }
        //      echo '<pre>categories'.$categories->id;
               //Check for swatches if exist then use or else create new one.
                $swatcheId = '';
                $finishcode =  $product[4];
                if(trim($product[4]) != '_') {

                 $swatches = \craft\elements\Entry::find()
                   ->section('swatches')
				   ->siteId(1)
                   ->where(['field_finishCode' =>  ['=', $finishcode]])
                   ->one();

                 if(!isset($swatches->id)) {
                   /*$section = Craft::$app->sections->getSectionByHandle('swatches');
                   $entryTypes = $section->getEntryTypes();
                   $entryType = reset($entryTypes);
                   $swatch = new \craft\elements\Entry([
                     'sectionId' => $section->id,
                     'typeId' => $entryType->id,
                     'fieldLayoutId' => $entryType->fieldLayoutId,
                     'authorId' => 1,
                     'title' => $product[4],
                     'slug' => $product[4],
                     'postDate' => new \DateTime(),
                   ]);

                   $swatch->setFieldValues([
                      'finishCode' => $finishcode
                   ]);

                   $success = Craft::$app->elements->saveElement($swatch, true, true, true);
                   $swatcheId = $swatch->id;*/
                 }
                 else {

                 /*  $swatches->setFieldValues([
                      'finishCode' => $product[3]
                   ]);
                   $success = Craft::$app->elements->saveElement($swatches);*/
                   $swatcheId = $swatches->id;
                 }

               }
               //echo $swatcheId;exit;
               //Set Metrix field
               $features = array();
               if(isset($product[8]) && $product[8] != '') {
                 $features['new1'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[8]
                   ]
                 ];
               }
               if(isset($product[9]) && $product[9] != '') {
                 $features['new2'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[9]
                   ]
                 ];
               }
               if(isset($product[10]) && $product[10] != '') {
                 $features['new3'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[10]
                   ]
                 ];
               }
               if(isset($product[11]) && $product[11] != '') {
                 $features['new4'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[11]
                   ]
                 ];
               }
               if(isset($product[12]) && $product[12] != '') {
                 $features['new5'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[12]
                   ]
                 ];
               }
               if(isset($product[13]) && $product[13] != '') {
                 $features['new6'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[13]
                   ]
                 ];
               }
               if(isset($product[14]) && $product[14] != '') {
                 $features['new7'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[14]
                   ]
                 ];
               }

            // $entries->productVariants
            $productVariants[$product[3]][] = [
              'type' => 'variants',
              'fields' => [
                  'swatches' => [$swatcheId],
                  'rrp' => $product[6],
                  'stockcode' => $product[1],
                  'leadTime' => $product[23],
              ]
            ];
            $c = 1;
            foreach ($productVariants[$product[3]] as $key => $value) {

              $productVariantsNew[$product[3]]['new'.$c] = $value;
              $c++;
            }
              $productStructure[$product[3]] = [
               "title" => $product[3],
               "desc" => $product[2],
               "swatches" => $productVariantsNew[$product[3]], //create swatch array,
               "features" => $features, //create features array,
               "category" =>  [$productCategory], //Create category array,
               "salesHtml" => $product[7],
              ];

          }

        }

      //  echo "<pre>"; print_r($productStructure); exit;

        foreach($productStructure as $product) {
          /*if($product['title'] == '055') {
            continue;
          }
          echo "<pre>"; print_r($product); exit;*/
          $entries = \craft\elements\Entry::find()
            ->section('products')
			->siteId(1)
            ->where(['title' => [ '=', $product['title']]])->one();
            if(!isset($entries->id)) {
              //add product
            //  echo  "new";
              //search for category and create if not exit.
              $section = Craft::$app->sections->getSectionByHandle('products');
              $entryTypes = $section->getEntryTypes();
              $entryType = reset($entryTypes);
              $entry = new \craft\elements\Entry([
                'sectionId' => $section->id,
                'typeId' => $entryType->id,
                'fieldLayoutId' => $entryType->fieldLayoutId,
                'authorId' => 1,
                'siteId' => 1,
                'title' =>  $product['title'],
                'slug' =>  $product['title'],
                'postDate' => new \DateTime(),
              ]);

              $entry->setFieldValues([
                 'productDescription' => $product['desc'],
                 'productVariants' => $product['swatches'],
                 'productCategory' =>  $product['category'],
                 'salesHtml' => $product['salesHtml'],
                 'features' => $product['features']
              ]);

              $success = Craft::$app->elements->saveElement($entry);
            //  exit;
            }
            else {
                //echo  "update";
                //Update entry if exist.
                //$entry = Entry::find()->id($entryId)->one();
                //$entries->title = $product[0]
                $productVariantsNew = [];
                $productVariants = $entries->productVariants->all();
                foreach ($productVariants as $variant) {
                    $c = 1;
                    $image = $variant->productImages;
                    $imageID = ($image != '' && isset($image[0])) ? $image[0]->id : [];
                    // $imageID = $image ? $image->id : [];
                    foreach ($product['swatches'] as $key => $value) {
                      if($value['fields']['stockcode'] == $variant->stockcode) {
                        $value['fields']['productImages'] = [$imageID];
						//$productVariantsNew['new'.$c] = $value;
						$product['swatches']['new'.$c]['fields']['productImages'] = [$imageID];
                      }
                      $c++;
                    }
					//$productVariantsNew = array_merge($product['swatches'], $productVariantsNew);
                    //echo "<pre>"; print_r(
					//$variant->productImages->one()); echo "-----";
                }

                $entries->setFieldValues([
                   'productDescription' => $product['desc'],
                   'productVariants' => $product['swatches'],
                   'productCategory' =>  $product['category'],
                   'salesHtml' => $product['salesHtml'],
                   'features' => $product['features']
                ]);

                $success = Craft::$app->elements->saveElement($entries);
                //exit;
            }
        }

      }catch(\Exception $e) {
        echo $e->getLine();
        echo $e->getMessage();
        print_r($e->getTrace());
        //exit;
      }
    }

    /**
     * Fetch category based on ajax.
     */
    public function actionGetAjaxCategory() {
      $request = Craft::$app->request->post();
      $categoryId = $request['categoryId'];
      $category = \craft\elements\Category::find()
        ->id($categoryId)
        ->one();
      $html = \Craft::$app->view->renderTemplate('/_entries/page/Product/ajax-category', ['category' => $category]);
		  return $this->asJson($html);
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/windsor-products/product/fetch-categories
     *
     * @return mixed
     */
    public function actionFetchCategories()
    {
      //ini_set('max_execution_time', 0);
      //set_time_limit(0);
      try {
        $username = 'gss';
        $password = '1912FrogMutiny';

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://exo.api.myob.com/stocksearchtemplate/24",
          CURLOPT_USERPWD => $username . ":" . $password,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "x-myobapi-exotoken:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
            "x-myobapi-key: 5de3bswg3ej8xxkcf5dynh39"
          ),
        ));

        $response = curl_exec($curl);
      //  echo curl_errno($curl);
      //  echo curl_error($curl);
        curl_close($curl);

        $categories = json_decode($response);
    //    echo "<pre>"; print_r($categories); exit;

        if($categories->data) {
          foreach ($categories->data as $key => $category) {
            $categoryParent = '';
            $categorySubParent = '';
            $categorySubParentTwo = '';

            if ($category[1] != '' && $category[1] != -1) {
               $categoriesData =  \craft\elements\Category::find()
                 ->group('productCategories')
				 ->siteId(1)
                 ->where(['content.field_categoryGroupNo' => $category[1].'-0-0-0'])->one();

                 if($categoriesData && $categoriesData->level == 1) {
                   //echo "<pre>"; print_r($categoriesData);
                   //Update category title here
                    $categoriesData->id = $categoriesData->id;
                    $categoriesData->categoryGroupNo = $category[1].'-0-0-0'; // works
                    $categoriesData->title = $category[2]; // works
                    $categoriesData->setScenario(Element::SCENARIO_LIVE);
                    $success = Craft::$app->elements->saveElement($categoriesData);
                    $categoryParent = $categoriesData->id;
                 }
                 else {
                   //Create new category
                    $group = Craft::$app->getCategories()->getGroupByHandle('productCategories');
                    $categoryNew = new \craft\elements\Category([
                      'groupId' => $group->id,
                      'fieldLayoutId' => $group->fieldLayoutId,
                      //'authorId' => 1,
					  'siteId' => 1,
                      'title' => $category[2],
                      'slug' => $category[2],
                      'level' => 1
                    ]);
                     $categoryNew->setFieldValues([
                      'categoryGroupNo' => $category[1].'-0-0-0'
                    ]);
                    $categoryNew->setScenario(Element::SCENARIO_LIVE);
                    $success = Craft::$app->elements->saveElement($categoryNew);
                    $categoryParent = $categoryNew->id;
                 }
                 //echo $categoryParent.'<br/>';
                 //check for child $categories
                 if ($category[3] != '' && $category[3] != -1) {
                   $categoryChildOne =  \craft\elements\Category::find()
                     ->group('productCategories')
					 ->siteId(1)
                     ->where(['content.field_categoryGroupNo' => $category[1].'-'.$category[3].'-0-0'])->one();
                  //   echo  'Category 2 => <pre>';print_r($categoryChildOne);
                      if($categoryChildOne && $categoryChildOne->level == 2) {
                        //Update categor with its parent.
                        $categoryChildOne->id = $categoryChildOne->id;
                        $categoryChildOne->categoryGroupNo = $category[1].'-'.$category[3].'-0-0'; // works
                        $categoryChildOne->title = $category[4]; // works
                        $categoryChildOne->newParentId =  $categoryParent;
                        $categoryChildOne->setScenario(Element::SCENARIO_LIVE);
                        $success = Craft::$app->elements->saveElement($categoryChildOne);
                        $categorySubParent = $categoryChildOne->id;
                      }
                      else {
                          //create category with its parent.
                          $group = Craft::$app->getCategories()->getGroupByHandle('productCategories');
                          $categoryNew = new \craft\elements\Category([
                            'groupId' => $group->id,
                            'fieldLayoutId' => $group->fieldLayoutId,
                            //'authorId' => 1,
							'siteId' => 1,
                            'title' => $category[4],
                           'slug' => $category[4],
                            'level' => 2
                          ]);
                           $categoryNew->setFieldValues([
                            'categoryGroupNo' => $category[1].'-'.$category[3].'-0-0'
                          ]);
                          $categoryNew->newParentId =  $categoryParent;
                          $categoryNew->setScenario(Element::SCENARIO_LIVE);
                          $success = Craft::$app->elements->saveElement($categoryNew);
                          $categorySubParent = $categoryNew->id;
                          //echo  'Category 2 => <pre>';print_r($success);
                      }

                      if ($category[5] != '' && $category[5] != -1) {

                        $categoryChildTwo =  \craft\elements\Category::find()
                          ->group('productCategories')
						  ->siteId(1)
                          ->where(['content.field_categoryGroupNo' => $category[1].'-'.$category[3].'-'.$category[5].'-0'])->one();
                           if($categoryChildTwo && $categoryChildTwo->level == 3) {
                             //Update categor with its parent.
                             $categoryChildTwo->id = $categoryChildTwo->id;
                             $categoryChildTwo->categoryGroupNo = $category[1].'-'.$category[3].'-'.$category[5].'-0'; // works
                             $categoryChildTwo->title = $category[6]; // works
                             $categoryChildTwo->newParentId =  $categorySubParent;
                             $categoryChildTwo->setScenario(Element::SCENARIO_LIVE);
                             $success = Craft::$app->elements->saveElement($categoryChildTwo);
                             $categorySubParentTwo = $categoryChildTwo->id;
                           }
                           else {
                             //create category with its parent.
                             $group = Craft::$app->getCategories()->getGroupByHandle('productCategories');
                             $categoryNew = new \craft\elements\Category([
                               'groupId' => $group->id,
                               'fieldLayoutId' => $group->fieldLayoutId,
                               //'authorId' => 1,
							   'siteId' => 1,
                               'title' => $category[6],
                              'slug' => $category[6],
                              'level' => 3
                             ]);
                              $categoryNew->setFieldValues([
                               'categoryGroupNo' => $category[1].'-'.$category[3].'-'.$category[5].'-0'
                             ]);
                             $categoryNew->newParentId = $categorySubParent;
                             $categoryNew->setScenario(Element::SCENARIO_LIVE);
                             $success = Craft::$app->elements->saveElement($categoryNew);
                             $categorySubParentTwo = $categoryNew->id;
                           }


                           if ($category[7] != '' && $category[7] != -1 && $category[7] != 0) {
                             $categoryChildThree =  \craft\elements\Category::find()
							   ->siteId(1)
                               ->group('productCategories')
                               ->where(['content.field_categoryGroupNo' => $category[1].'-'.$category[3].'-'.$category[5].'-'.$category[7]])->one();
                                if($categoryChildThree && $categoryChildThree->level == 4) {
                                  //Update categor with its parent.
                                  $categoryChildThree->id = $categoryChildThree->id;
                                  $categoryChildThree->categoryGroupNo = $category[1].'-'.$category[3].'-'.$category[5].'-'.$category[7]; // works
                                  $categoryChildThree->title = $category[8]; // works
                                  $categoryChildThree->newParentId = $categorySubParentTwo;
                                  $categoryChildThree->setScenario(Element::SCENARIO_LIVE);
                                  $success = Craft::$app->elements->saveElement($categoryChildThree);
                                  //  $categorySubParentTwo = $categoryChildTwo;
                                  //$productCategory = $categoryChildThree->id;
                                }
                                else {
                                  //create category with its parent.
                                  $group = Craft::$app->getCategories()->getGroupByHandle('productCategories');
                                  $categoryNew = new \craft\elements\Category([
                                    'groupId' => $group->id,
                                    'fieldLayoutId' => $group->fieldLayoutId,
                                    //'authorId' => 1,
									'siteId' => 1,
                                    'title' => $category[8],
                                    'slug' => $category[8],
                                    'level' => 4
                                  ]);
                                   $categoryNew->setFieldValues([
                                    'categoryGroupNo' => $category[1].'-'.$category[3].'-'.$category[5].'-'.$category[7]
                                  ]);
                                  $categoryNew->newParentId = $categorySubParentTwo;
                                  $categoryNew->setScenario(Element::SCENARIO_LIVE);
                                  $success = Craft::$app->elements->saveElement($categoryNew);
                                  //$categorySubParentTwo = $category;
                                  //$productCategory = $category->id;
                                }
                            }
                       }
                  }
                // echo "<pre>"; print_r($categories); exit;
             }
          }
        }
      }
      catch(\Exception $e) {
      echo $e->getLine();
      echo $e->getMessage();
      print_r($e->getTrace());
      //exit;
    }
  }

  /**
   * Handle a request going to our plugin's actionDoSomething URL,
   * e.g.: actions/windsor-products/product/delete-categories
   *
   * @return mixed
   */
  public function actionDeleteCategories()
  {
    //ini_set('max_execution_time', 0);
    //set_time_limit(0);
    try {
      $username = 'gss';
      $password = '1912FrogMutiny';

      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://exo.api.myob.com/stocksearchtemplate/24",
        CURLOPT_USERPWD => $username . ":" . $password,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "Accept: application/json",
          "x-myobapi-exotoken:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
          "x-myobapi-key: 5de3bswg3ej8xxkcf5dynh39"
        ),
      ));

      $response = curl_exec($curl);
    //  echo curl_errno($curl);
    //  echo curl_error($curl);
      curl_close($curl);

      $categories = json_decode($response);
  //    echo "<pre>"; print_r($categories); exit;
  $categoriesAll =  \craft\elements\Category::find()
	->siteId(2)
    ->group('productCategories')->all();


      $categoryLive = [] ;
  //  Craft::$app->getElements()->deleteElement($categoryNew);
      if($categories->data) {
        foreach ($categories->data as $key => $category) {

          if ($category[1] != '' && $category[1] != -1) {
               $categoryLive[] = $category[1].'-0-0-0';
          }
//create array from db
//create array from Live
//compare array
//fetch query from category
//call delete

          if ($category[3] != '' && $category[3] != -1) {
            $categoryLive[] = $category[1].'-'.$category[3].'-0-0';
          }
          if ($category[5] != '' && $category[5] != -1) {
              $categoryLive[] = $category[1].'-'.$category[3].'-'.$category[5].'-0';
          }
          if ($category[7] != '' && $category[7] != -1 && $category[7] != 0) {
             $categoryLive[] = $category[1].'-'.$category[3].'-'.$category[5].'-'.$category[7];
          }
        }
      }

      foreach ($categoriesAll as $cat) {
          if (!in_array($cat->categoryGroupNo ,$categoryLive)) {
            Craft::$app->getElements()->deleteElement($cat);
          }
      }
    }
    catch(\Exception $e) {
    echo $e->getLine();
    echo $e->getMessage();
    print_r($e->getTrace());
    //exit;
  }
}
/**
 * Handle a request going to our plugin's actionDoSomething URL,
 * e.g.: actions/windsor-products/product/delete-products
 *
 * @return mixed
 */
public function actionDeleteProducts()
{
  /*$productsAll = \craft\elements\Entry::find()
    ->section('products')
    ->where(['title' => [ '=', '2037']])->one();
    //->limit(null)
    //->all();
    echo '<pre>';print_r($productsAll->productVariants);
    foreach ($productsAll->productVariants as $value) {
      echo '<pre>Variant => ';print_r($value);
    }

    exit;*/
  ini_set('max_execution_time', 0);
  //set_time_limit(0);
  try {
    $username = 'gss';
    $password = '1912FrogMutiny';

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://exo.api.myob.com/stocksearchtemplate/25",
      CURLOPT_USERPWD => $username . ":" . $password,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_SSL_VERIFYHOST => 0,
      CURLOPT_SSL_VERIFYPEER => 0,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "x-myobapi-exotoken:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
        "x-myobapi-key: 5de3bswg3ej8xxkcf5dynh39"
      ),
    ));

    $response = curl_exec($curl);
    //echo curl_errno($curl);
    // echo curl_error($curl);
    curl_close($curl);

    $products = json_decode($response);

    $productVariantsNew = [];
    $productsNew = [];

    //echo "<pre>"; print_r($productsAll);exit;

    if($products->data) {
      foreach ($products->data as $key => $product) {
        // $entries->productVariants
        $productVariantsNew[] = $product[1];
        $productsNew[] = $product[3];
      }
    }

    $productsAll = \craft\elements\Entry::find()
      ->section('products')
	  ->siteId(1)
      //->where(['title' => [ '=', '2037']])->one();
      //->limit(null)
      ->all();

    //echo "<pre>"; print_r($productsAll);//exit;

    foreach($productsAll as $product) {
      //echo "<pre>"; print_r($product);
      foreach ($product->productVariants as $variant) {
          if (!in_array($variant->stockcode, $productVariantsNew)) {
            // delete metrix
            //echo '<pre>VVV=>';print_r($variant);
            unset($variant);
            Craft::$app->elements->saveElement($product);
            //print_r($matrixBlock);
          }
      }
      if(!in_array($product->title, $productsNew)) {
        Craft::$app->getElements()->deleteElement($product);
      }
    }

  }catch(\Exception $e) {
    echo $e->getLine();
    echo $e->getMessage();
    print_r($e->getTrace());
    //exit;
  }
}


/**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/windsor-products/product/aus-fetch-products
     *
     * @return mixed
     */
    public function actionAusFetchProducts()
    {

      ini_set('max_execution_time', 0);
      //set_time_limit(0);
      try {
        $username = 'gss';
        $password = '1912FrogMutiny';

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://exo.api.myob.com/stocksearchtemplate/27",
          CURLOPT_USERPWD => $username . ":" . $password,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "x-myobapi-exotoken:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
            "x-myobapi-key: 5de3bswg3ej8xxkcf5dynh39"
          ),
        ));

        $response = curl_exec($curl);
        //echo curl_errno($curl);
        // echo curl_error($curl);
        curl_close($curl);

        $products = json_decode($response);
        //echo "<pre>"; print_r($products); exit;
        $productStructure = array();
        //check product variants exist or not if not then add/or Update
        $productVariants = [];
        $productVariantsNew = [];

        if($products->data) {
          foreach ($products->data as $key => $product) {

              //Checking for category exist or we have to create one.
            // if ($key > 10) {
              // continue;
            //  }


        //Prepare data here only and create in next loop.
              $australianProductCategory = '';
              $categories =  \craft\elements\Category::find()
              ->group('australianProductCategories')
			        ->siteId(2)
              ->where(['content.field_categoryGroupNo' => trim($product[0])])->one();

              if($categories && $categories->id){

				$australianProductCategory = $categories->id;
			  }
        //      echo '<pre>categories'.$categories->id;
               //Check for swatches if exist then use or else create new one.
                $swatcheId = '';
                $finishcode =  $product[4];
                if(trim($product[4]) != '_') {

                 $swatches = \craft\elements\Entry::find()
                   ->section('swatches')
				   ->siteId(2)
                   ->where(['field_finishCode' =>  ['=', $finishcode]])
                   ->one();

                 if(!isset($swatches->id)) {
                   /*$section = Craft::$app->sections->getSectionByHandle('swatches');
                   $entryTypes = $section->getEntryTypes();
                   $entryType = reset($entryTypes);
                   $swatch = new \craft\elements\Entry([
                     'sectionId' => $section->id,
                     'typeId' => $entryType->id,
                     'fieldLayoutId' => $entryType->fieldLayoutId,
                     'authorId' => 1,
                     'title' => $product[4],
                     'slug' => $product[4],
                     'postDate' => new \DateTime(),
                   ]);

                   $swatch->setFieldValues([
                      'finishCode' => $finishcode
                   ]);

                   $success = Craft::$app->elements->saveElement($swatch, true, true, true);
                   $swatcheId = $swatch->id;*/
                 }
                 else {

                 /*  $swatches->setFieldValues([
                      'finishCode' => $product[3]
                   ]);
                   $success = Craft::$app->elements->saveElement($swatches);*/
                   $swatcheId = $swatches->id;
                 }

               }
               //echo $swatcheId;exit;
               //Set Metrix field
               $features = array();
               if(isset($product[8]) && $product[8] != '') {
                 $features['new1'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[8]
                   ]
                 ];
               }
               if(isset($product[9]) && $product[9] != '') {
                 $features['new2'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[9]
                   ]
                 ];
               }
               if(isset($product[10]) && $product[10] != '') {
                 $features['new3'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[10]
                   ]
                 ];
               }
               if(isset($product[11]) && $product[11] != '') {
                 $features['new4'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[11]
                   ]
                 ];
               }
               if(isset($product[12]) && $product[12] != '') {
                 $features['new5'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[12]
                   ]
                 ];
               }
               if(isset($product[13]) && $product[13] != '') {
                 $features['new6'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[13]
                   ]
                 ];
               }
               if(isset($product[14]) && $product[14] != '') {
                 $features['new7'] = [
                   'type' => 'blockContent',
                   'fields' => [
                       'feature' => $product[14]
                   ]
                 ];
               }

            // $entries->productVariants
            $productVariants[$product[3]][] = [
              'type' => 'variants',
              'fields' => [
                  'swatches' => [$swatcheId],
                  'rrp' => $product[6],
                  'stockcode' => $product[1],
                  'leadTime' => $product[23],
              ]
            ];
            $c = 1;
            foreach ($productVariants[$product[3]] as $key => $value) {

              $productVariantsNew[$product[3]]['new'.$c] = $value;
              $c++;
            }
              $productStructure[$product[3]] = [
               "title" => $product[3],
               "desc" => $product[2],
               "swatches" => $productVariantsNew[$product[3]], //create swatch array,
               "features" => $features, //create features array,
               "category" =>  [$australianProductCategory], //Create category array,
               "salesHtml" => $product[7],
              ];

          }

        }

      //  echo "<pre>"; print_r($productStructure); exit;

        foreach($productStructure as $product) {
          /*if($product['title'] == '055') {
            continue;
          }
          echo "<pre>"; print_r($product); exit;*/
          $entries = \craft\elements\Entry::find()
            ->section('products')
			         ->siteId(2)
            ->where(['title' => [ '=', $product['title']]])->one();
            if(!isset($entries->id)) {
              //add product
            //  echo  "new";
              //search for category and create if not exit.
              $section = Craft::$app->sections->getSectionByHandle('products');
              $entryTypes = $section->getEntryTypes();
              $entryType = reset($entryTypes);
              $entry = new \craft\elements\Entry([
                'sectionId' => $section->id,
                'typeId' => $entryType->id,
                'fieldLayoutId' => $entryType->fieldLayoutId,
                'authorId' => 1,
				            'siteId' => 2,
                'title' =>  $product['title'],
                'slug' =>  $product['title'],
                'postDate' => new \DateTime(),
              ]);

              $entry->setFieldValues([
                 'productDescription' => $product['desc'],
                 'productVariants' => $product['swatches'],
                 'australianProductCategory' =>  $product['category'],
                 'salesHtml' => $product['salesHtml'],
                 'features' => $product['features']
              ]);

              $success = Craft::$app->elements->saveElement($entry);
            //  exit;
            }
            else {
                //echo  "update";
                //Update entry if exist.
                //$entry = Entry::find()->id($entryId)->one();
                //$entries->title = $product[0]
                $productVariantsNew = [];
                $productVariants = $entries->productVariants->all();
                foreach ($productVariants as $variant) {
                    $c = 1;
                    $image = $variant->productImages;
                    $imageID = ($image != '' && isset($image[0])) ? $image[0]->id : [];
                    // $imageID = $image ? $image->id : [];
                    foreach ($product['swatches'] as $key => $value) {
                      if($value['fields']['stockcode'] == $variant->stockcode) {
                        $value['fields']['productImages'] = [$imageID];
						//$productVariantsNew['new'.$c] = $value;
						$product['swatches']['new'.$c]['fields']['productImages'] = [$imageID];
                      }
                      $c++;
                    }
					//$productVariantsNew = array_merge($product['swatches'], $productVariantsNew);
                    //echo "<pre>"; print_r(
					//$variant->productImages->one()); echo "-----";
                }

                $entries->setFieldValues([
                   'productDescription' => $product['desc'],
                   'productVariants' => $product['swatches'],
                   'australianProductCategory' =>  $product['category'],
                   'salesHtml' => $product['salesHtml'],
                   'features' => $product['features']
                ]);

                $success = Craft::$app->elements->saveElement($entries);
                //exit;
            }
        }

      }catch(\Exception $e) {
        echo $e->getLine();
        echo $e->getMessage();
        print_r($e->getTrace());
        //exit;
      }
    }



    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/windsor-products/product/aus-fetch-categories
     *
     * @return mixed
     */
    public function actionAusFetchCategories()
    {
      //ini_set('max_execution_time', 0);
      //set_time_limit(0);
      try {
        $username = 'gss';
        $password = '1912FrogMutiny';

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://exo.api.myob.com/stocksearchtemplate/26",
          CURLOPT_USERPWD => $username . ":" . $password,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "x-myobapi-exotoken:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
            "x-myobapi-key: 5de3bswg3ej8xxkcf5dynh39"
          ),
        ));

        $response = curl_exec($curl);
      //  echo curl_errno($curl);
      //  echo curl_error($curl);
        curl_close($curl);

        $categories = json_decode($response);
    //    echo "<pre>"; print_r($categories); exit;

        if($categories->data) {
          foreach ($categories->data as $key => $category) {
            $categoryParent = '';
            $categorySubParent = '';
            $categorySubParentTwo = '';

            if ($category[1] != '' && $category[1] != -1) {
               $categoriesData =  \craft\elements\Category::find()
                 ->group('australianProductCategories')
				 ->siteId(2)
                 ->where(['content.field_categoryGroupNo' => $category[1].'-0-0-0'])->one();

                 if($categoriesData && $categoriesData->level == 1) {
                   //echo "<pre>"; print_r($categoriesData);
                   //Update category title here
                    $categoriesData->id = $categoriesData->id;
                    $categoriesData->categoryGroupNo = $category[1].'-0-0-0'; // works
                    $categoriesData->title = $category[2]; // works
                    $categoriesData->setScenario(Element::SCENARIO_LIVE);
                    $success = Craft::$app->elements->saveElement($categoriesData);
                    $categoryParent = $categoriesData->id;
                 }
                 else {
                   //Create new category
                    $group = Craft::$app->getCategories()->getGroupByHandle('australianProductCategories');
                    $categoryNew = new \craft\elements\Category([
                      'groupId' => $group->id,
                      'fieldLayoutId' => $group->fieldLayoutId,
                      //'authorId' => 1,
					  'siteId' => 2,
                      'title' => $category[2],
                      'slug' => $category[2],
                      'level' => 1
                    ]);
                     $categoryNew->setFieldValues([
                      'categoryGroupNo' => $category[1].'-0-0-0'
                    ]);
                    $categoryNew->setScenario(Element::SCENARIO_LIVE);
                    $success = Craft::$app->elements->saveElement($categoryNew);
                    $categoryParent = $categoryNew->id;
                 }
                 //echo $categoryParent.'<br/>';
                 //check for child $categories
                 if ($category[3] != '' && $category[3] != -1) {
                   $categoryChildOne =  \craft\elements\Category::find()
                     ->group('australianProductCategories')
					 ->siteId(2)
                     ->where(['content.field_categoryGroupNo' => $category[1].'-'.$category[3].'-0-0'])->one();
                  //   echo  'Category 2 => <pre>';print_r($categoryChildOne);
                      if($categoryChildOne && $categoryChildOne->level == 2) {
                        //Update categor with its parent.
                        $categoryChildOne->id = $categoryChildOne->id;
                        $categoryChildOne->categoryGroupNo = $category[1].'-'.$category[3].'-0-0'; // works
                        $categoryChildOne->title = $category[4]; // works
                        $categoryChildOne->newParentId =  $categoryParent;
                        $categoryChildOne->setScenario(Element::SCENARIO_LIVE);
                        $success = Craft::$app->elements->saveElement($categoryChildOne);
                        $categorySubParent = $categoryChildOne->id;
                      }
                      else {
                          //create category with its parent.
                          $group = Craft::$app->getCategories()->getGroupByHandle('australianProductCategories');
                          $categoryNew = new \craft\elements\Category([
                            'groupId' => $group->id,
                            'fieldLayoutId' => $group->fieldLayoutId,
                            //'authorId' => 1,
							'siteId' => 2,
                            'title' => $category[4],
                           'slug' => $category[4],
                            'level' => 2
                          ]);
                           $categoryNew->setFieldValues([
                            'categoryGroupNo' => $category[1].'-'.$category[3].'-0-0'
                          ]);
                          $categoryNew->newParentId =  $categoryParent;
                          $categoryNew->setScenario(Element::SCENARIO_LIVE);
                          $success = Craft::$app->elements->saveElement($categoryNew);
                          $categorySubParent = $categoryNew->id;
                          //echo  'Category 2 => <pre>';print_r($success);
                      }

                      if ($category[5] != '' && $category[5] != -1) {

                        $categoryChildTwo =  \craft\elements\Category::find()
                          ->group('australianProductCategories')
						  ->siteId(2)
                          ->where(['content.field_categoryGroupNo' => $category[1].'-'.$category[3].'-'.$category[5].'-0'])->one();
                           if($categoryChildTwo && $categoryChildTwo->level == 3) {
                             //Update categor with its parent.
                             $categoryChildTwo->id = $categoryChildTwo->id;
                             $categoryChildTwo->categoryGroupNo = $category[1].'-'.$category[3].'-'.$category[5].'-0'; // works
                             $categoryChildTwo->title = $category[6]; // works
                             $categoryChildTwo->newParentId =  $categorySubParent;
                             $categoryChildTwo->setScenario(Element::SCENARIO_LIVE);
                             $success = Craft::$app->elements->saveElement($categoryChildTwo);
                             $categorySubParentTwo = $categoryChildTwo->id;
                           }
                           else {
                             //create category with its parent.
                             $group = Craft::$app->getCategories()->getGroupByHandle('australianProductCategories');
                             $categoryNew = new \craft\elements\Category([
                               'groupId' => $group->id,
                               'fieldLayoutId' => $group->fieldLayoutId,
                               //'authorId' => 1,
							   'siteId' => 2,
                               'title' => $category[6],
                              'slug' => $category[6],
                              'level' => 3
                             ]);
                              $categoryNew->setFieldValues([
                               'categoryGroupNo' => $category[1].'-'.$category[3].'-'.$category[5].'-0'
                             ]);
                             $categoryNew->newParentId = $categorySubParent;
                             $categoryNew->setScenario(Element::SCENARIO_LIVE);
                             $success = Craft::$app->elements->saveElement($categoryNew);
                             $categorySubParentTwo = $categoryNew->id;
                           }


                           if ($category[7] != '' && $category[7] != -1 && $category[7] != 0) {
                             $categoryChildThree =  \craft\elements\Category::find()
                               ->group('australianProductCategories')
							   ->siteId(2)
                               ->where(['content.field_categoryGroupNo' => $category[1].'-'.$category[3].'-'.$category[5].'-'.$category[7]])->one();
                                if($categoryChildThree && $categoryChildThree->level == 4) {
                                  //Update categor with its parent.
                                  $categoryChildThree->id = $categoryChildThree->id;
                                  $categoryChildThree->categoryGroupNo = $category[1].'-'.$category[3].'-'.$category[5].'-'.$category[7]; // works
                                  $categoryChildThree->title = $category[8]; // works
                                  $categoryChildThree->newParentId = $categorySubParentTwo;
                                  $categoryChildThree->setScenario(Element::SCENARIO_LIVE);
                                  $success = Craft::$app->elements->saveElement($categoryChildThree);
                                  //  $categorySubParentTwo = $categoryChildTwo;
                                  //$productCategory = $categoryChildThree->id;
                                }
                                else {
                                  //create category with its parent.
                                  $group = Craft::$app->getCategories()->getGroupByHandle('australianProductCategories');
                                  $categoryNew = new \craft\elements\Category([
                                    'groupId' => $group->id,
                                    'fieldLayoutId' => $group->fieldLayoutId,
                                    //'authorId' => 1,
									'siteId' => 2,
                                    'title' => $category[8],
                                    'slug' => $category[8],
                                    'level' => 4
                                  ]);
                                   $categoryNew->setFieldValues([
                                    'categoryGroupNo' => $category[1].'-'.$category[3].'-'.$category[5].'-'.$category[7]
                                  ]);
                                  $categoryNew->newParentId = $categorySubParentTwo;
                                  $categoryNew->setScenario(Element::SCENARIO_LIVE);
                                  $success = Craft::$app->elements->saveElement($categoryNew);
                                  //$categorySubParentTwo = $category;
                                  //$productCategory = $category->id;
                                }
                            }
                       }
                  }
                // echo "<pre>"; print_r($categories); exit;
             }
          }
        }
      }
      catch(\Exception $e) {
      echo $e->getLine();
      echo $e->getMessage();
      print_r($e->getTrace());
      //exit;
    }
  }

  /**
   * Handle a request going to our plugin's actionDoSomething URL,
   * e.g.: actions/windsor-products/product/aus-delete-categories
   *
   * @return mixed
   */
  public function actionAusDeleteCategories()
  {
    //ini_set('max_execution_time', 0);
    //set_time_limit(0);
    try {
      $username = 'gss';
      $password = '1912FrogMutiny';

      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://exo.api.myob.com/stocksearchtemplate/26",
        CURLOPT_USERPWD => $username . ":" . $password,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "Accept: application/json",
          "x-myobapi-exotoken:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
          "x-myobapi-key: 5de3bswg3ej8xxkcf5dynh39"
        ),
      ));

      $response = curl_exec($curl);
    //  echo curl_errno($curl);
    //  echo curl_error($curl);
      curl_close($curl);

      $categories = json_decode($response);
  //    echo "<pre>"; print_r($categories); exit;
  $categoriesAll =  \craft\elements\Category::find()
	->siteId(2)
    ->group('australianProductCategories')->all();


      $categoryLive = [] ;
  //  Craft::$app->getElements()->deleteElement($categoryNew);
      if($categories->data) {
        foreach ($categories->data as $key => $category) {

          if ($category[1] != '' && $category[1] != -1) {
               $categoryLive[] = $category[1].'-0-0-0';
          }
//create array from db
//create array from Live
//compare array
//fetch query from category
//call delete

          if ($category[3] != '' && $category[3] != -1) {
            $categoryLive[] = $category[1].'-'.$category[3].'-0-0';
          }
          if ($category[5] != '' && $category[5] != -1) {
              $categoryLive[] = $category[1].'-'.$category[3].'-'.$category[5].'-0';
          }
          if ($category[7] != '' && $category[7] != -1 && $category[7] != 0) {
             $categoryLive[] = $category[1].'-'.$category[3].'-'.$category[5].'-'.$category[7];
          }
        }
      }

      foreach ($categoriesAll as $cat) {
          if (!in_array($cat->categoryGroupNo ,$categoryLive)) {
            Craft::$app->getElements()->deleteElement($cat);
          }
      }
    }
    catch(\Exception $e) {
    echo $e->getLine();
    echo $e->getMessage();
    print_r($e->getTrace());
    //exit;
  }
}
/**
 * Handle a request going to our plugin's actionDoSomething URL,
 * e.g.: actions/windsor-products/product/aus-delete-products
 *
 * @return mixed
 */
public function actionAusDeleteProducts()
{
  /*$productsAll = \craft\elements\Entry::find()
    ->section('products')
    ->where(['title' => [ '=', '2037']])->one();
    //->limit(null)
    //->all();
    echo '<pre>';print_r($productsAll->productVariants);
    foreach ($productsAll->productVariants as $value) {
      echo '<pre>Variant => ';print_r($value);
    }

    exit;*/
  ini_set('max_execution_time', 0);
  //set_time_limit(0);
  try {
    $username = 'gss';
    $password = '1912FrogMutiny';

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://exo.api.myob.com/stocksearchtemplate/27",
      CURLOPT_USERPWD => $username . ":" . $password,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_SSL_VERIFYHOST => 0,
      CURLOPT_SSL_VERIFYPEER => 0,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "x-myobapi-exotoken:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
        "x-myobapi-key: 5de3bswg3ej8xxkcf5dynh39"
      ),
    ));

    $response = curl_exec($curl);
    //echo curl_errno($curl);
    // echo curl_error($curl);
    curl_close($curl);

    $products = json_decode($response);

    $productVariantsNew = [];
    $productsNew = [];

    //echo "<pre>"; print_r($productsAll);exit;

    if($products->data) {
      foreach ($products->data as $key => $product) {
        // $entries->productVariants
        $productVariantsNew[] = $product[1];
        $productsNew[] = $product[3];
      }
    }

    $productsAll = \craft\elements\Entry::find()
      ->section('products')
	  ->siteId(2)
      //->where(['title' => [ '=', '2037']])->one();
      //->limit(null)
      ->all();

    //echo "<pre>"; print_r($productsAll);//exit;

    foreach($productsAll as $product) {
      //echo "<pre>"; print_r($product);
      foreach ($product->productVariants as $variant) {
          if (!in_array($variant->stockcode, $productVariantsNew)) {
            // delete metrix
            //echo '<pre>VVV=>';print_r($variant);
            unset($variant);
            Craft::$app->elements->saveElement($product);
            //print_r($matrixBlock);
          }
      }
      if(!in_array($product->title, $productsNew)) {
        Craft::$app->getElements()->deleteElement($product);
      }
    }

  }catch(\Exception $e) {
    echo $e->getLine();
    echo $e->getMessage();
    print_r($e->getTrace());
    //exit;
  }
}

  /**
   * Handle a request going to our plugin's index action URL,
   * e.g.: actions/windsor-products/product/test-scripts
   *
   * @return mixed
   */
  public function actionTestScripts() {
    /*$entries = \craft\elements\Entry::find()
      ->section('products')
      ->where(['title' => [ '=', '8113']])->one();
      $productVariants = $entries->productVariants->all();
      foreach ($productVariants as $variant) {
        if($variant->stockcode == '8113-BLK') {
          $productVariants[$product[3]][] = [
            'type' => 'variants',
            'fields' => [
                'swatches' => [$swatcheId],
                'rrp' => $product[6],
                'stockcode' => $product[1],
                'leadTime' => $product[23],
            ]
          ];
          $c = 1;
          $image = $variant->productImages->one();
          $imageID = ($image) ? $image->id() : [];
          foreach ($productVariants[$product[3]] as $key => $value) {
            if($value['fields']['stockcode'] == $variant->stockcode) {
              $value['fields']['productImages'] = [$imageID];
            }
            $productVariantsNew[$product[3]]['new'.$c] = $value;
            $c++;
          }
          echo "<pre>"; print_r($variant->productImages->one()); echo "-----";
        }
      }
      exit;
      echo "<pre>dd"; print_r($productVariants); exit;*/
	  /*$entries->setFieldValues([
                   //'productDescription' => $product['desc'],
                   //'productVariants' => $product['swatches'],
                   'productCategory' =>  [361684],
                  // 'salesHtml' => $product['salesHtml'],
                   //'features' => $product['features']
                ]);

                $success = Craft::$app->elements->saveElement($entries);*/
  }
}
