<?php
/**
 * Windsor Products plugin for Craft CMS 3.x
 *
 * Fetch products from backend
 *
 * @link      https://www.hotzone.in
 * @copyright Copyright (c) 2020 Priti Rathod
 */

/**
 * Windsor Products en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('windsor-products', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Priti Rathod
 * @package   WindsorProducts
 * @since     1.0.0
 */
return [
    'Windsor Products plugin loaded' => 'Windsor Products plugin loaded',
];
