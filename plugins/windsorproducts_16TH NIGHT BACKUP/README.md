# Windsor Products plugin for Craft CMS 3.x

Fetch products from backend

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /windsor-products

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Windsor Products.

## Windsor Products Overview

-Insert text here-

## Configuring Windsor Products

-Insert text here-

## Using Windsor Products

-Insert text here-

## Windsor Products Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Priti Rathod](https://www.hotzone.in)
