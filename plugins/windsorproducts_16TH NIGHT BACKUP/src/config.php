<?php
/**
 * Windsor Products plugin for Craft CMS 3.x
 *
 * Fetch products from backend
 *
 * @link      https://www.hotzone.in
 * @copyright Copyright (c) 2020 Priti Rathod
 */

/**
 * Windsor Products config.php
 *
 * This file exists only as a template for the Windsor Products settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'windsor-products.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "someAttribute" => true,

];
