<?php
/**
 * Windsor Products plugin for Craft CMS 3.x
 *
 * Fetch products from backend
 *
 * @link      https://www.hotzone.in
 * @copyright Copyright (c) 2020 Priti Rathod
 */

namespace windsor\windsorproducts\variables;

use windsor\windsorproducts\WindsorProducts;

use Craft;

/**
 * Windsor Products Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.windsorProducts }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Priti Rathod
 * @package   WindsorProducts
 * @since     1.0.0
 */
class WindsorProductsVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.windsorProducts.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.windsorProducts.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
}
