{% extends '_layouts/base' %}

{% block bodyContent %}

	{{ parent() }}

  <div class="w-full pl-12 md:pl-16">


  	{% for image in globalBlackLogo.logo.all() %}
  		<div id="logo" class="w-full absolute top-0">
  			<a href="{{ url('') }}">
  				<img class="pt-8 sm:pl-20 mx-auto sm:mx-0 w-48 sm:w-64" src="{{ image.getUrl() }}" alt="Windsor logo"/>
  			</a>
  		</div>
  	{% endfor %}

		<style>
		.loc-list .list {
			height: 70vh;
			overflow-y: scroll;
			border: 1px solid #d4d4d4;
		}
		.loc-list .list li {
		    padding: 10px;
		    border-bottom: 1px solid #d4d4d4;
		}
		.autocomplete-items {
		  position: absolute;
		  border: 1px solid #d4d4d4;
		  border-radius:0 0 4px 4px;
		  border-top: none;
		  z-index: 99;
		  /*position the autocomplete items to be the same width as the container:*/
		  top: 42.9%;
		  left: 15%;
		  right: 0;
			width:300px;
			max-height: 168px;
			overflow-y: scroll;
			display: none;
			background-color: #fff;
		}

		.autocomplete-items p{
		  padding: 10px;
		  cursor: pointer;
		  background-color: #fff;
		  border-bottom: 1px solid #d4d4d4;
		}
		#map-container{
			display: none;
		}
		.reset-map-zoom{
	    position: absolute;
	    right: 64.5%;
	    bottom: -23%;
	    z-index: 9999;
	    background: #fff;
	    background: none rgb(255, 255, 255);
	    border: 0px;
	    margin: 10px;
	    padding: 5px 10px;
	    position: absolute;
	    cursor: pointer;
	    user-select: none;
	    border-radius: 2px;
	    /* height: 40px; */
	    /* width: 40px; */
	    box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px;
	    overflow: hidden;
	    /* top: 0px; */
	    /* right: 0px; */
	    font-weight: 500;
		}
		</style>
  	<section class="w-full flex flex-col justify-center items-center pt-32 px-6 py-12">
      <h1 data-aos="fade-up" class="leading-tight text-4xl md:text-5xl font-hairline text-center mb-6">
      		{{entry.title}}
      </h1>

      <div class="p-6 lg:px-24 w-full">
          <!-- Title -->
          <div class="p-back w-full mb-6">
            <p
              class="text-center uppercase tracking-wider text-lg text-white py-2 px-3 mb-12"
            >
              Find a stockist
            </p>
          </div>

          <!-- Address Bar -->
          <p class="text-xs mb-2 font-base">
            Enter your address or city in the field to find your nearest
            stockist:
          </p>
          <form onsubmit="return false" class="flex flex-row border text-sm rounded-full max-w-sm w-full py-2 mb-12">
            <div class="px-4">Maps</div>
            <input
              id="autocomplete"
              placeholder="Address or City"
							class="flex-1 px-3 focus:outline-none border-l-2"
              type="text"
            />
          {#}  <input
              class="flex-1 px-3 focus:outline-none border-l-2"
              placeholder="Address or City"
              type="text"
              onFocus="geolocate()"
            />#}

          </form>
					<div class="autocomplete-items"></div>
        </div>
    </section>
		<!-- Special MAP -->
     <div id="map-container" class="w-full pl-16 md:pl-0 hidden">
			 <div class="loc-list sm:w-1/3 md:w-1/2 lg:w-3/12 object-cover px-6 sm:px-0 flex-shrink-0">
           <ul class="list"></ul>
       </div>
			 <div id="map" style="height:70vh;" class="loc-list sm:w-1/3 md:w-1/2 lg:w-9/12 object-cover px-6 sm:px-0 flex-shrink-0"></div>
					<button class="reset-map-zoom" onclick="resetMapZoom()"><span>Reset Zoom</span></button>
     </div>
    <!-- Landing Image -->
      <div
        data-aos="fade-up"
        data-aos-anchor-placement="top-bottom"
        class="w-full pl-16 md:pl-0"
				id="image-container"
      >
        <img src="/assets/images/headerBanner.png" alt="test" />
      </div>
  </div>

  {% endblock %}
  {% block customJs%}
  <!--<script>
      // This sample uses the Autocomplete widget to help the user select a
      // place, then it retrieves the address components associated with that
      // place, and then it populates the form fields with those details.
      // This sample requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script
      // src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;

      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'), {types: ['geocode']});

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields(['address_component']);

        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle(
                {center: geolocation, radius: position.coords.accuracy});
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
  	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeA1h11Yjt0A6qB-Sk1h3VMxwiDbx31NQ&libraries=places&callback=initAutocomplete"
        async defer></script>-->

<script type="text/javascript">
/*function initmap(mapdata) {
			var defaultZoomedIn = 13;
			var defaultZoomedOut = 5;
      //Initialize the Google Maps Windsor -36.820193, 174.755067
      var geocoder;
      var map;
      var markersArray = [];
      var infos = [];
      var latlng = new google.maps.LatLng(-36.820193, 174.755067);
      geocoder = new google.maps.Geocoder();
      var myOptions = {
            zoom: 5,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
						styles:[
								    {
								        "featureType": "administrative",
								        "elementType": "labels.text.fill",
								        "stylers": [
								            {
								                "color": "#444444"
								            }
								        ]
								    },
								    {
								        "featureType": "landscape",
								        "elementType": "all",
								        "stylers": [
								            {
								                "color": "#eeeeee"
								            }
								        ]
								    },
								    {
								        "featureType": "landscape",
								        "elementType": "geometry",
								        "stylers": [
								            {
								                "saturation": "0"
								            }
								        ]
								    },
								    {
								        "featureType": "poi",
								        "elementType": "all",
								        "stylers": [
								            {
								                "visibility": "off"
								            }
								        ]
								    },
								    {
								        "featureType": "road",
								        "elementType": "all",
								        "stylers": [
								            {
								                "saturation": -100
								            },
								            {
								                "lightness": 45
								            }
								        ]
								    },
								    {
								        "featureType": "road.highway",
								        "elementType": "all",
								        "stylers": [
								            {
								                "visibility": "simplified"
								            }
								        ]
								    },
								    {
								        "featureType": "road.arterial",
								        "elementType": "labels.icon",
								        "stylers": [
								            {
								                "visibility": "off"
								            }
								        ]
								    },
								    {
								        "featureType": "transit",
								        "elementType": "all",
								        "stylers": [
								            {
								                "visibility": "off"
								            }
								        ]
								    },
								    {
								        "featureType": "water",
								        "elementType": "all",
								        "stylers": [
								            {
								                "color": "#f6f6f6"
								            },
								            {
								                "visibility": "on"
								            }
								        ]
								    },
								    {
								        "featureType": "water",
								        "elementType": "geometry.fill",
								        "stylers": [
								            {
								                "saturation": "-11"
								            },
								            {
								                "lightness": "3"
								            },
								            {
								                "gamma": "2.05"
								            },
								            {
								                "weight": "0.01"
								            }
								        ]
								    }
								]
          }
      //Load the Map into the map_canvas div
      var map = new google.maps.Map(document.getElementById("map"), myOptions);

			window.map = map;
	    window.defaultZoomedOut = defaultZoomedOut;
      //map = new google.maps.Map(document.getElementById("map"), myOptions);
      //Initialize a variable that the auto-size the map to whatever you are plotting
      var bounds = new google.maps.LatLngBounds();
      //Initialize the encoded string
      //var encodedString = mapdata;
      //var stringArray=JSON.parse( encodedString );
			var stock_list = '';
      var stringArray=mapdata;
      var x;
      for (x = 0; x < stringArray.length; x = x + 1)
      {
					var stocklat = stringArray[x]['latitude'];
					var stocklng = stringArray[x]['longitude'];
					var address = stringArray[x]['address'];//"Auckland";
					var accno = stringArray[x]['accno'];
					var website = stringArray[x]['website'];
					var phone = stringArray[x]['phone'];

					//
					/*latlong_address(geocoder,address,function(stocklatlng){
						console.log('Map '+stocklatlng);
						stocklat = stocklatlng[0];
						stocklng = stocklatlng[1];
					});*/

					//var stocklatlng = geo_latlong(address);
					//var stocklat = stocklatlng[0];
					//var stocklng = stocklatlng[1];

					/*var addresssettings = {
				  	"url": "https://maps.googleapis.com/maps/api/geocode/json?address=Manukau&sensor=false&key=AIzaSyCeA1h11Yjt0A6qB-Sk1h3VMxwiDbx31NQ",
					  "method": "GET",
					  "timeout": 0,
					};

					$.ajax(addresssettings).done(function (data) {

						stocklat = data.results[0].geometry.location.lat;
						stocklng = data.results[0].geometry.location.lng;
						console.log(stocklat+" = "+address+" = "+stocklng);
					});*/

/*					geocoder.geocode( { 'address': address}, function(results, status,phone,website,address) {
					  if (status == google.maps.GeocoderStatus.OK) {
					    var stocklat = results[0].geometry.location.lat();
					    var stocklng = results[0].geometry.location.lng();

					//console.log('Map '+stocklatlng);
					console.log(stocklat+" = "+address+" = "+stocklng);
					//

          var marker;
          //Load the lat, long data
          var lat = new google.maps.LatLng(stocklat,stocklng);
					//console.log("= "+lat);
					var markerinfo = '<div class="map-div">'+
													 '<div class="map-inner">'+
													 '<p class="map-p"><strong>'+name+'</strong></p>'+
													 '<p class="map-p">Address: '+address+'</p>';
							if(phone != ''){
								markerinfo += '<p class="map-p">Phone: '+phone+'</p>';
							}
							if(website != ''){
								var websiteurl = website;
								var pattern = /^((http|https|ftp):\/\/)/;
								if(!pattern.test(websiteurl)) {
	    						websiteurl = "http://" + websiteurl;
								}
								markerinfo +=	'<p class="map-p">Website: <a target="_blank" href="'+websiteurl+'">'+website+'</a></p>';
							}
							markerinfo +=	'</div></div>';


          //Create a new marker and info window
          marker = new google.maps.Marker({
              map: map,
              position: lat,
              //url: "#",
              //Content is what will show up in the info window
              content: markerinfo
          });

					stock_list += '<li>'+marker.content+'</li>';
          //Pushing the markers into an array so that it's easier to manage them
          markersArray.push(marker);
          /*google.maps.event.addListener( marker, 'mouseover', function () {
              closeInfos();
              var info = new google.maps.InfoWindow({content: this.content});
              //On click the map will load the info window
              info.open(map,this);
              infos[0]=info;
          });*/

	/*				google.maps.event.addListener(marker, 'click', (function(marker) {

	              return function() {
									closeInfos();
		              var info = new google.maps.InfoWindow({content: this.content});
		              //On click the map will load the info window
		              info.open(map,this);
		              infos[0]=info;
	                map.panTo(marker.position);
	                map.setZoom(defaultZoomedIn);
	              }

	        })(marker));

					google.maps.event.addListener(map, 'zoom_changed', function() {

	            if (map.getZoom() < defaultZoomedIn) {
	              if (!stringArray.length) {
	                return;
	              }
	          		closeInfos();
	            }

	        });
          //google.maps.event.addListener( marker, 'mouseout', function () {
            //  closeInfos();
          //});
          //google.maps.event.addListener( marker, 'click', function () {
          //  window.open(this.url,'_blank');
          //});
         //Extends the boundaries of the map to include this new location
         //bounds.extend(lat);
			 		} // end of status geocoder
			 	});
      }
      //Takes all the lat, longs in the bounds variable and autosizes the map
      //map.fitBounds(bounds);
			$('.loc-list .list').html(stock_list);
      //Manages the info windows
    function closeInfos(){
      if(infos.length > 0){
        infos[0].set("marker",null);
        infos[0].close();
        infos.length = 0;
      }
    }
}

function resetMapZoom() {
	window.map.setZoom(window.defaultZoomedOut);
}
function geo_latlong(address){
	var latlngarray = [];
	//latlngarray[0] = -36.820193;
	//latlngarray[1] = 174.755067;
	//var address='55 Lambie Drive, Manukau, Auckland, 2104';//address which you want Longitude and Latitude
	address= address.replace(" ", "+")
	$.ajax({
		type: "GET",
		dataType: "json",
		url: "https://maps.googleapis.com/maps/api/geocode/json",
		data: {'address': address,'sensor':false,key:'AIzaSyCeA1h11Yjt0A6qB-Sk1h3VMxwiDbx31NQ'},
		success: function(data){

		    if(data.results.length){
						latlngarray.push(data.results[0].geometry.location.lat);
						latlngarray.push(data.results[0].geometry.location.lng);
						console.log('1'+latlngarray);
						return latlngarray;
		    }
		}
	});
	console.log('2'+latlngarray);
	return latlngarray;
}
//var geocoder2;
function latlong_address(geocoder2,address,callback) {
			var stocklatlong = [];
			//stocklatlong['latitude'] = '123';
			//stocklatlong['longitude'] = '123';
			//geocoder2 = new google.maps.Geocoder();
			geocoder2.geocode( { 'address': address}, function(results, status) {
			  if (status == google.maps.GeocoderStatus.OK) {
			    var latitude = results[0].geometry.location.lat();
			    var longitude = results[0].geometry.location.lng();
					stocklatlong.push(latitude);
					stocklatlong.push(longitude);
					//showResult(stocklatlong);
					//stocklatlong['latitude'] = latitude;
					//stocklatlong['longitude'] = longitude;
					callback(stocklatlong);
			  }
			});
			//console.log(stocklatlong);
			//return stocklatlong;
}*/
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeA1h11Yjt0A6qB-Sk1h3VMxwiDbx31NQ&&libraries=places" async defer></script>
<script>
		function xmlToJson(xml) {
		  // Create the return object
		  var obj = {};

		  if (xml.nodeType == 1) {
		    // element
		    // do attributes
		    if (xml.attributes.length > 0) {
		      obj["@attributes"] = {};
		      for (var j = 0; j < xml.attributes.length; j++) {
		        var attribute = xml.attributes.item(j);
		        obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
		      }
		    }
		  } else if (xml.nodeType == 3) {
		    // text
		    obj = xml.nodeValue;
		  }

		  // do children
		  // If all text nodes inside, get concatenated text from them.
		  var textNodes = [].slice.call(xml.childNodes).filter(function(node) {
		    return node.nodeType === 3;
		  });
		  if (xml.hasChildNodes() && xml.childNodes.length === textNodes.length) {
		    obj = [].slice.call(xml.childNodes).reduce(function(text, node) {
		      return text + node.nodeValue;
		    }, "");
		  } else if (xml.hasChildNodes()) {
		    for (var i = 0; i < xml.childNodes.length; i++) {
		      var item = xml.childNodes.item(i);
		      var nodeName = item.nodeName;
		      if (typeof obj[nodeName] == "undefined") {
		        obj[nodeName] = xmlToJson(item);
		      } else {
		        if (typeof obj[nodeName].push == "undefined") {
		          var old = obj[nodeName];
		          obj[nodeName] = [];
		          obj[nodeName].push(old);
		        }
		        obj[nodeName].push(xmlToJson(item));
		      }
		    }
		  }
		  return obj;
		}

		function getObjects(obj, key, val) {
		    var objects = [];
		    for (var i in obj) {
		        if (!obj.hasOwnProperty(i)) continue;
		        if (typeof obj[i] == 'object') {
		            objects = objects.concat(getObjects(obj[i], key, val));
		        } else if (i == key && obj[key] == val) {
		            objects.push(obj);
		        }
		    }
		    return objects;
		}

		var settings = {
		  "url": "https://exo.api.myob.com/stocksearchtemplate/23",
		  "method": "GET",
		  "timeout": 0,
		  "headers": {
		    "x-myobapi-exotoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY3Mzc2MDAsImlzcyI6IlMtMS01LTIxLTM0NjYxMzc0MDMtMTQyNDc0NjQxOS02MzEyNjU5NzI6L3o0TnZzRWxMUno1aDhsbm5Ia1gzUT09IiwiYXVkIjoiaHR0cHM6Ly9leG8uYXBpLm15b2IuY29tLyIsIm5hbWUiOiJHU1MiLCJzdGFmZm5vIjoiNzMiLCJhcHBpZCI6IjQ0MDAifQ.kGfjO341AsDptY0GXvhtsb0ZWR3y9ddkS0yDMZ5fots",
		    "x-myobapi-key": "5de3bswg3ej8xxkcf5dynh39",
		    "Authorization": "Basic Z3NzOjE5MTJGcm9nTXV0aW55"
		  },
		};

		$.ajax(settings).done(function (response) {
			var obj = xmlToJson(response);
		  //console.log(obj.SearchTemplateResultSet.Data);
			//console.log(getObjects(obj,1, 'Albany'));
			var stockist = obj.SearchTemplateResultSet.Data;

			function searched_stocklist(type){
				//$('#autocomplete').on('keyup focus',function(){
	        var searchField = $('#autocomplete').val();
					if(searchField === '')  {
						$('.autocomplete-items').html('');
						$('.autocomplete-items').hide();
						$('#map-container').css('display','none');
						$('#image-container').show();
						return;
					}

	        var regex = new RegExp(searchField, "i");
	        //var output = '<div class="row">';
					var output = '';
	        var count = 0;
					var stockdata = new Array();
				  $.each(stockist, function(key, val){
						/*if ((typeof val.Data[3] == 'string' && val.Data[3].search(regex) != -1)
								|| (typeof val.Data[4] == 'string' && val.Data[4].search(regex) != -1)
								|| (typeof val.Data[5] == 'string' && val.Data[5].search(regex) != -1)
								|| (typeof val.Data[6] == 'string' && val.Data[6].search(regex) != -1)) {*/

								//console.log(val.Data[3]);
								var autocomplete_address = '';

								if(typeof val.Data[3] == 'string'){
									autocomplete_address += val.Data[3] + ', '
								}
								if(typeof val.Data[4] == 'string'){
									autocomplete_address += val.Data[4] + ', '
								}
								if(typeof val.Data[5] == 'string'){
									autocomplete_address += val.Data[5] + ', '
								}
								if(typeof val.Data[6] == 'string'){
									autocomplete_address += val.Data[6]
								}

								autocomplete_address = autocomplete_address.replace(/,+/g,',');
  							autocomplete_address = autocomplete_address.replace(/,\s*$/, "");
								autocomplete_address = autocomplete_address.trim();

							if (autocomplete_address.search(regex) != -1){

								output += '<p class="autocomplete-address">'+autocomplete_address+'</p>'

								var phone = '';
								var website = '';
								var stock_lat = -36.820193;
								var stock_lng = 174.755067;

								if(typeof val.Data[2] == 'string'){
									phone = val.Data[2];
								}
								if(typeof val.Data[7] == 'string'){
									website = val.Data[7];
								}

								// mapdata //

								stockdata[count] = {
									accno : val.Data[0],
									name : val.Data[1],
									phone : phone,
									address : autocomplete_address,
									website : website,
									latitude : stock_lat,
									longitude : stock_lng
								};

						  	count++;
							}
				  });
						//console.log(stockdata);
				  	//output += '</div>';
						$('.autocomplete-items').html(output);
						$('.autocomplete-items').show();
						if(type == 2){
							initmap(stockdata);
						}
						$('.autocomplete-address').on('click', function() {
						  $('#autocomplete').val($(this).text());
							$('#autocomplete').focus();
							searched_stocklist(2);
							//initmap(stockdata);
							$('#map-container').css('display','flex');
							$('#image-container').hide();
							$('.autocomplete-items').hide();
							//console.log('hello');
						});
	    	//});
				}
				$('#autocomplete').on('keyup focus',function(){
						searched_stocklist(1);
				});
				$('#autocomplete').on('mouseout',function(){
						//$('.autocomplete-items').hide();
				});
				$(document).on('keypress',function(e) {
						if(e.which == 13) {
							$('#map-container').css('display','flex');
							$('#image-container').hide();
							//initmap(stockdata);
							searched_stocklist(2);
							$("#autocomplete").blur();
							$('.autocomplete-items').hide();
						}
						//alert('You pressed enter!');
				});
});
$('body').on('click',function(e) {
	//$('.autocomplete-items').hide();
});
</script>
<script type="text/javascript">
function initmap(locations){
	var defaultZoomedIn = 13;
	var defaultZoomedOut = 8;
  //Initialize the Google Maps Windsor -36.820193, 174.755067
	var markers = [];
  var infos = [];
	var geocoder;
	var map;
	var bounds = new google.maps.LatLngBounds();
	var stock_list = '';
	function initialize() {
	    map = new google.maps.Map(
	    document.getElementById("map"), {
	        center: new google.maps.LatLng(-36.820193, 174.755067),
	        zoom: defaultZoomedOut,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
					styles:[
								    {
								        "featureType": "administrative",
								        "elementType": "labels.text.fill",
								        "stylers": [
								            {
								                "color": "#444444"
								            }
								        ]
								    },
								    {
								        "featureType": "landscape",
								        "elementType": "all",
								        "stylers": [
								            {
								                "color": "#eeeeee"
								            }
								        ]
								    },
								    {
								        "featureType": "landscape",
								        "elementType": "geometry",
								        "stylers": [
								            {
								                "saturation": "0"
								            }
								        ]
								    },
								    {
								        "featureType": "poi",
								        "elementType": "all",
								        "stylers": [
								            {
								                "visibility": "off"
								            }
								        ]
								    },
								    {
								        "featureType": "road",
								        "elementType": "all",
								        "stylers": [
								            {
								                "saturation": -100
								            },
								            {
								                "lightness": 45
								            }
								        ]
								    },
								    {
								        "featureType": "road.highway",
								        "elementType": "all",
								        "stylers": [
								            {
								                "visibility": "simplified"
								            }
								        ]
								    },
								    {
								        "featureType": "road.arterial",
								        "elementType": "labels.icon",
								        "stylers": [
								            {
								                "visibility": "off"
								            }
								        ]
								    },
								    {
								        "featureType": "transit",
								        "elementType": "all",
								        "stylers": [
								            {
								                "visibility": "off"
								            }
								        ]
								    },
								    {
								        "featureType": "water",
								        "elementType": "all",
								        "stylers": [
								            {
								                "color": "#f6f6f6"
								            },
								            {
								                "visibility": "on"
								            }
								        ]
								    },
								    {
								        "featureType": "water",
								        "elementType": "geometry.fill",
								        "stylers": [
								            {
								                "saturation": "-11"
								            },
								            {
								                "lightness": "3"
								            },
								            {
								                "gamma": "2.05"
								            },
								            {
								                "weight": "0.01"
								            }
								        ]
								    }
								]
	    });
	    geocoder = new google.maps.Geocoder();
	    window.map = map;
	    window.defaultZoomedOut = defaultZoomedOut;
	    for (i = 0; i < locations.length; i++) {
	        geocodeAddress(locations, i);
	    }
	    google.maps.event.addListener(map, 'zoom_changed', function() {
	        if (map.getZoom() < defaultZoomedIn) {
	          	if (!locations.length) {
	            	return;
	          	}
	      		closeInfos();
	        }
	    });
	}

	//Manages the info windows
    function closeInfos(){
      if(infos.length > 0){
        infos[0].set("marker",null);
        infos[0].close();
        infos.length = 0;
      }
    }
	//google.maps.event.addDomListener(window, "load", initialize);

	function geocodeAddress(locations, i) {
		var stocklat = locations[i]['latitude'];
		var stocklng = locations[i]['longitude'];
		var address = locations[i]['address'];//"Auckland";
		var accno = locations[i]['accno'];
	  var phone = locations[i]['phone'];
	  var website = locations[i]['website'];
	  var name = locations[i]['name'];
	    geocoder.geocode({
	        'address': locations[i]['address']
	    },

	    function (results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	            var marker = new google.maps.Marker({
	                icon: '/assets/images/Elements/Windsor-Map-Location-Icon.png',//'https://maps.google.com/mapfiles/ms/icons/blue.png',
	                map: map,
	                position: results[0].geometry.location,
	                title: name,
	                animation: google.maps.Animation.DROP,
	                address: address,
	                //url: url
	            })
	            infoWindow(marker, map, name, address, phone, website);
	          	stocklist(name, address, phone, website);
	            bounds.extend(marker.getPosition());
	            map.fitBounds(bounds);
							markers.push(marker);
	        } else {
	            //alert("geocode of " + address + " failed:" + status);
	        }
	    });
	}
	function stocklist(name, address, phone, website){
		stock_list += '<li>'+listofstock(name, address, phone, website)+'</li>';
		//console.log(stock_list);
		$('.loc-list .list').html(stock_list);
	}
	function infoWindow(marker, map, name, address, phone, website) {
	    google.maps.event.addListener(marker, 'click', function () {
				closeInfos();
				for (var j = 0; j < markers.length; j++) {
            markers[j].setIcon("/assets/images/Elements/Windsor-Map-Location-Icon.png");
        }
	    	var html = listofstock(name, address, phone, website);
				marker.setIcon('/assets/images/Elements/Windsor-Map-Location-Icon_SELECTED.png');
	        //var html = "<div><h3>" + title + "</h3><p>" + address + "<br></div><a href='" + url + "'>View location</a></p></div>";
	        iw = new google.maps.InfoWindow({
	            content: html,
	            maxWidth: 350
	        });
	        iw.open(map, marker);
	        infos[0]=iw;
	        map.panTo(marker.position);
	        map.setZoom(defaultZoomedIn);
	    });
	}

	/*function createMarker(results) {
	    var marker = new google.maps.Marker({
	        icon: 'https://maps.google.com/mapfiles/ms/icons/blue.png',
	        map: map,
	        position: results[0].geometry.location,
	        title: name,
	        animation: google.maps.Animation.DROP,
	        address: address,
	        //url: url
	    })
	    bounds.extend(marker.getPosition());
	    map.fitBounds(bounds);
	    infoWindow(marker, map, name, address, phone, website);
	    return marker;
	}*/
	function listofstock(name, address, phone, website) {
	    var markerinfo = '<div class="map-div">'+
		 		'<div class="map-inner">'+
		 		'<p class="map-p"><strong>'+name+'</strong></p>'+
		 		'<p class="map-p">Address: '+address+'</p>';
				if(phone != ''){
					markerinfo += '<p class="map-p"><a href="tel:'+phone+'">Phone: '+phone+'</a></p>';
				}
				if(website != ''){
					var websiteurl = website;
					var pattern = /^((http|https|ftp):\/\/)/;
					if(!pattern.test(websiteurl)) {
						websiteurl = "http://" + websiteurl;
					}
					markerinfo +=	'<p class="map-p">Website: <a target="_blank" href="'+websiteurl+'">'+website+'</a></p>';
				}
			markerinfo +=	'</div></div>';
	     return markerinfo;
	}
	initialize();
}
function resetMapZoom() {
	window.map.setZoom(window.defaultZoomedOut);
}
</script>
{% endblock %}
