// const navOverlay = document.querySelector('.nav-overlay');
// const searchOverlay = document.querySelector('.search-overlay');
// const navIcon = document.querySelector('.nav-icon');
//
// function active() {
//   navOverlay.classList.toggle('nav-overlay-active');
//   navIcon.classList.toggle('is-active');
// }
//
// function searchActive() {
//   searchOverlay.classList.add('search-overlay-active');
//   navOverlay.classList.remove('nav-overlay-active');
//   // navIcon.classList.remove('is-active');
//   navIcon.classList.remove('is-active');
//   document.getElementById('searchInput').focus();
// }
//
// function remove() {
//   searchOverlay.classList.remove('search-overlay-active');
// }

const navOverlay = document.querySelector('.nav-overlay');
const countryOverlay = document.querySelector('.country-overlay');
const searchOverlay = document.querySelector('.search-overlay');
const navIcon = document.querySelector('.nav-icon');
const countryIcon = document.querySelector('.country-icon');

function active() {
  navOverlay.classList.toggle('nav-overlay-active');
  navIcon.classList.toggle('is-active');
  countryOverlay.classList.remove('country-overlay-active');
  countryIcon.classList.remove('is-active')
}

function countryActive() {
  countryOverlay.classList.toggle('country-overlay-active');
  countryIcon.classList.toggle('is-active');
  navOverlay.classList.remove('nav-overlay-active');
  navIcon.classList.remove('is-active');
}

function searchActive() {
  searchOverlay.classList.add('search-overlay-active');
  searchOverlay.classList.toggle('is-active');
  navOverlay.classList.remove('nav-overlay-active');
  // navIcon.classList.remove('is-active');
  navIcon.classList.remove('is-active');
  countryOverlay.classList.remove('country-overlay-active');
  countryIcon.classList.remove('is-active')
  document.getElementById('searchInput').focus();
}

function remove() {
  searchOverlay.classList.remove('search-overlay-active');
}


const tabItems = document.querySelectorAll('.tab-item');
const tabContentItems = document.querySelectorAll('.tab-content-item');

// Select tab content item
function selectItem(e) {
	// Remove all show and border classes
	removeBorder();
	removeShow();
	// Add border to current tab item
	this.classList.add('tab-border');
	// Grab content item from DOM
	const tabContentItem = document.querySelector(`#${this.id}-content`);
	// Add show class
	tabContentItem.classList.add('show');
}

// Remove bottom borders from all tab items
function removeBorder() {
	tabItems.forEach(item => {
		item.classList.remove('tab-border');
	});
}

// Remove show class from all content items
function removeShow() {
	tabContentItems.forEach(item => {
		item.classList.remove('show');
	});
}

// Listen for tab item click
tabItems.forEach(item => {
	item.addEventListener('click', selectItem);
});


let tabLine = $('.tab-line');
let tabOne = document.querySelector('#tab-1');
let tabTwo = document.querySelector('#tab-2');
let tabThree = document.querySelector('#tab-3');

function toOne() {
  // tabLine.style.marginLeft = "0rem";
  tabLine.style.marginLeft = "0";
  tabLine.classList.add('tab-active');
  tabTwo.classList.remove('tab-active');
  tabThree.classList.remove('tab-active');
}
function toTwo() {
  tabLine.style.marginLeft = "3rem";
  tabOne.classList.remove('tab-active');
  tabTwo.classList.add('tab-active');
  tabThree.classList.remove('tab-active');
}
function toThree() {
  tabLine.style.marginLeft = "6rem";
  tabOne.classList.remove('tab-active');
  tabTwo.classList.remove('tab-active');
  tabThree.classList.add('tab-active');
}

$(".tab-item").on("click", function(){
  $(".tab-item").removeClass("tab-active");
  $('.tab-line').hide();
  $('.tab-content-item').removeClass('show');
  $(this).find('.tab-line').show();
  $(this).addClass('tab-active');
  $("#"+$(this).attr('id')+"-content").addClass('show');

});

// Back to top

(function() {
	var backTop = document.getElementsByClassName('js-back-to-top')[0];
	if( backTop ) {
	  var dataElement = backTop.getAttribute('data-element');
	  var scrollElement = dataElement ? document.querySelector(dataElement) : window;
	  var scrollDuration = parseInt(backTop.getAttribute('data-duration')) || 300, //scroll to top duration
		scrollOffset = parseInt(backTop.getAttribute('data-offset')) || 0, //show back-to-top if scrolling > scrollOffset
		scrolling = false;

	  //detect click on back-to-top link
	  backTop.addEventListener('click', function(event) {
		event.preventDefault();
		if(!window.requestAnimationFrame) {
		  scrollElement.scrollTo(0, 0);
		} else {
		  dataElement ? Util.scrollTo(0, scrollDuration, false, scrollElement) : Util.scrollTo(0, scrollDuration);
		}
		//move the focus to the #top-element - don't break keyboard navigation
		Util.moveFocus(document.getElementById(backTop.getAttribute('href').replace('#', '')));
	  });

	  //listen to the window scroll and update back-to-top visibility
	  checkBackToTop();
	  if (scrollOffset > 0) {
		scrollElement.addEventListener("scroll", function(event) {
		  if( !scrolling ) {
			scrolling = true;
			(!window.requestAnimationFrame) ? setTimeout(function(){checkBackToTop();}, 250) : window.requestAnimationFrame(checkBackToTop);
		  }
		});
	  }

	  function checkBackToTop() {
		var windowTop = scrollElement.scrollTop || document.documentElement.scrollTop;
		if(!dataElement) windowTop = window.scrollY || document.documentElement.scrollTop;
		Util.toggleClass(backTop, 'back-to-top--is-visible', windowTop >= scrollOffset);
		scrolling = false;
	  }
	}
  }());

  // Back to Top

var btn = $('#button');
var btnT = $('#button-2')

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
	btn.addClass('appear');
	btnT.removeClass('appear')
  } else {
	btn.removeClass('appear');
	btnT.addClass('appear')
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});


  /* Demo purposes only */
  $(".hover").mouseleave(
    function () {
      $(this).removeClass("hover");
    }
  );

/* swatch open and close */

$(".swatch-box-left").on("click", function(){
    if($(this).hasClass("active")) {
        $(".standard-swatch-description-left").hide();
        $(".swatch-box-left").removeClass("active");
    }
    else {
      $(".standard-swatch-description-left").hide();
      $(".swatch-box-left").removeClass("active");
      $(this).addClass("active");
      $("."+$(this).attr('data-swatch-left')).show();
    }

});

$(".swatch-box-right").on("click", function(){
    if($(this).hasClass("active")) {
        $(".standard-swatch-description-right").hide();
        $(".swatch-box-right").removeClass("active");
    }
    else {
      $(".standard-swatch-description-right").hide();
      $(".swatch-box-right").removeClass("active");
      $(this).addClass("active");
      $("."+$(this).attr('data-swatch-right')).show();
    }

});


//Checkbox-Radio


$('input[type=radio]').bind('touchstart mousedown', function() {
this.checked = !this.checked
}).bind('click touchend', function(e) {
e.preventDefault()
});

// Video Script for Installation page

$(".open-video").on("click", function(e){
  e.preventDefault();
  var href= $(this).attr('data-video');
  var iframe = '<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><iframe src="'+ href +'" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="100%" height="100%"></iframe></div></div><script src="https://fast.wistia.net/assets/external/E-v1.js" async></script>';
  $(".video-modal").show();
  $(".video-modal .video-content").html(iframe);
  $(".overlay-modal").show();
  $("body").css("overflow-y", "hidden");
});
$(".video-close").on("click", function(){
  $(".video-modal").hide();
  $(".video-modal .video-content").html('');
  $(".overlay-modal").hide();
  $("body").css("overflow-y", "auto");
});


$(".product-category-level-two li").on("click", function(){
  var category = $(this).attr('data-category');

  $.ajax({
    url: "/get-ajax-category",
    type: "POST",
    data:  {'categoryId': category, 'CRAFT_CSRF_TOKEN' : csrfTokenValue},
    success: function(result){
      $([document.documentElement, document.body]).animate({
        scrollTop: $(".childCategory-level-3").offset().top
      }, 500);
     $(".childCategory-level-3").html(result);
   }
 });
});


let nav = document.querySelector('nav');
let main = document.querySelector('#main')
let modal = document.querySelector('.modal');

function openModal() {
  nav.classList.add('hidden');
//  main.classList.add('hidden');
  modal.classList.remove('hidden');

  let currentIndex = $('#carouselExampleIndicators').find('.carousel-item.active').index() - 1;
  $("#carouselExampleIndicators2").find('.carousel-item').removeClass('active');
  $("#carouselExampleIndicators2").find('.carousel-item').eq(currentIndex).addClass('active');

  // alert("small:"+ currentIndex);
  // alert("bif:"+  $('#carouselExampleIndicators2').find('.carousel-item.active').index());
}

function closeModal() {
  nav.classList.remove('hidden');
  //main.classList.remove('hidden');
  modal.classList.add('hidden');

  let currentIndex = $('#carouselExampleIndicators2').find('.carousel-item.active').index();
  $("#carouselExampleIndicators").find('.carousel-item').removeClass('active');
  $("#carouselExampleIndicators").find('.carousel-item').eq(currentIndex).addClass('active');
}


$(".child-category").on("click", function(e){
  e.preventDefault();
  var category = $(this).attr('data-category');
  if ($(".cat-"+category).hasClass("active")) {
    $(".child-category-data").hide();
    $(".child-category-data").removeClass('active');
//    $(".cat-"+category).html('');
  }else {

    $(".child-category-data").hide();
    $(".child-category-data").removeClass('active');
    $(".cat-"+category).show();
    $(".cat-"+category).addClass('active');
  /*  $.ajax({
      url: "/ajax/get-fitting-category?categoryId="+category,
      type: "GET",
      success: function(result){
        $(".child-category-data").hide();
        $(".child-category-data").removeClass('active');
        $(".cat-"+category).show();
        $(".cat-"+category).addClass('active');
        $(".cat-"+category).html(result);
     }
   });*/
 }
});

$(".product-category-level-two li").hover(function(){
  $(".category-info").hide();
  let category = $(this).attr("data-category");
  $(".category-info-"+category).show();
},
function() { //$(".category-info").hide();
});
