$(document).ready(function(){ 
 
	if($('.youtubeVideoEmbed').length > 0)
	{
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
 
		//Config
		window.loopSeconds 	= 60; 
        
		var players = [];
  
		//When the iframe is ready hit play ;)
		window.onPlayerReady = function(event) {

			event.target.playVideo();
			event.target.mute();
		}

		window.onYouTubeIframeAPIReady = function() {
 
			$('.youtubeVideoEmbed').each(function(index, element){
  
				var playerId 	= $(element).attr('id');
				var videoId 	= $(element).attr('videoId');
				var videoHeight = $(element).attr('videoHeight');
    
				players[playerId] = new YT.Player(playerId, {
					height: videoHeight,  
					width: '100%',
					videoId: videoId,
					playerVars: {
						rel:0,							// Show related videos 
						autoplay: 1,            		// Auto-play the video on load
						controls: 0,            		// Show pause/play buttons in player
						showinfo: 0,            		// Hide the video title
						modestbranding: 1,      		// Hide the Youtube Logo
						fs: 1,                  		// Hide the full screen button
						cc_load_policy: 1,      		// Hide closed captions
						iv_load_policy: 3,      		// Hide the Video Annotations
						start: 0,						// Start timestamp in seconds
						end: window.loopSeconds,		// End timestamp in seconds
						autohide: 0, 					// Hide video controls when playing
					},
					events: {
						'onReady': window.onPlayerReady,
						'onStateChange': window.onPlayerStateChange
					}
				});

			});

		}
 
		//Do things		
		window.onPlayerStateChange = function(event) {

   			//Loop video
			if (event.data == YT.PlayerState.ENDED) event.target.playVideo();

		}
		
	}

});